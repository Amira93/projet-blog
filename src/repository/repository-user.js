import { user } from "../entity/user";
import { Connection } from "./connection";



export class userRepository {

    static async findAll() {
        const [rows] = await Connection.query("SELECT * FROM user")

        const utilisateur = [];
        for (const row of rows) {
            let instance = new user(row.name, row.email, row.password, row.role, row.id)
            utilisateur.push(instance)
        }
        return utilisateur;
    }

    static async findByEmail(email) {
        const [rows] = await Connection.query("SELECT * FROM user WHERE email = ?", [email]);
        if (rows.length === 1) {
            return new user(rows[0].name, rows[0].email, rows[0].password, rows[0].role, rows[0].id)
        }
        return null
    }

    static async addUser(user) {
        const [rows] = await Connection.query("INSERT INTO user (name, email, password, role) VALUE (?,?,?,?)", [user.name, user.email, user.password, user.role]);
        user.id = rows.insertId;
    }

    static async search(term) {
        const [rows] = await Connection.query('SELECT * FROM user WHERE CONCAT(name, email) LIKE ?', ['%' + term + '%'])
        const use = [];
        for (const row of rows) {
            let ins = new user(row.name, row.email, row.password, row.role, row.id);
            use.push(ins);
        }
        return use;
    }

}