import { createPool } from "mysql2/promise";


export const Connection = createPool(process.env.DATABASE_URL)