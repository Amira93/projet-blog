import { article } from "../entity/article";
import { user } from "../entity/user";
import { Connection } from "./connection";


export class articleRepository {

    static async findAllArticleUser() {
        let art = []
            const [rows] = await Connection.query("SELECT *, article.id AS articleid FROM article INNER JOIN user ON id_user = user.id ");
        for (const row of rows) {
            let instance = new article(row.titre, row.contenu, row.id_user, row.image, row.articleid)
            instance.user = new user(row.name, row.email, row.password, row.role, row.id_user)
            art.push(instance)
        }
        return art;
    
    }

    static async findById(id)
    {
        const [rows] = await Connection.query("SELECT * FROM article WHERE id = ?",[id]);
        const art = [];
        for (const row of rows) {
            let instance = new article(row.titre, row.contenu, row.id_user, row.image, row.id)
            art.push(instance);
        }
        return art
    }

    // static async findByIdUser(idUser) {
    //     const [rows] = await Connection.query("SELECT * FROM article WHERE id_user = ?", [idUser]);
    //     const art = [];
    //     for (const row of rows) {
    //         let instance = new article(row.titre, row.contenu, row.id_user, row.image, row.id)
    //         art.push(instance);
    //     }
    //     return art
    // }

    static async addArticle(article) {
        const [rows] = await Connection.query("INSERT INTO article(titre, contenu, id_user, image) VALUE (?,?,?,?)", [article.titre, article.contenu, article.id_user, article.image])
        article.id = rows.insertId;
    }

    static async search(term){
        const [rows] = await Connection.query('SELECT * , article.id AS articleid FROM article INNER JOIN user ON CONCAT(titre, id_user) LIKE ?', ['%'+term+'%'])
        const art = [];
        for (const row of rows) {
            let ins = new article(row.titre, row.contenu, row.id_user, row.image, row.articleid);
            ins.user = new user(row.name, row.email, row.password, row.role, row.id_user)
            art.push(ins);
        }
        return art;
    }

    static async updateArticle(article) {
        await Connection.query('UPDATE article SET titre=?, contenu=?, id_user=?, image=? WHERE id=?', [article.titre, article.contenu, article.id_user, article.image, article.id]);
    }

    static async deleteArticle(id) {
        await Connection.query('DELETE FROM article WHERE id = ?', [id]);
    }

}