import express from 'express';
import cors from 'cors';
import { userController } from './controller/controller-user';
import { articleController } from './controller/controller-article';
import { configurePassport } from './utils/token';
import  Passport  from 'passport';

configurePassport()
export const server = express();

server.use(Passport.initialize())

server.use(express.json());
server.use(cors());

server.use("/api/user", userController);
server.use("/api/article", articleController);
// server.use(express.static('public'));
