import { Router } from "express"
import { user } from "../entity/user";
import { userRepository } from "../repository/repository-user";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from 'passport';



export const userController = Router();

userController.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let user;
        if (req.query.search) {
            user = await userRepository.search(req.query.search);
        } else {
            user = await userRepository.findAll();
        }

        res.json(user);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);

    }
})

userController.post('/', async (req, res) => {
    try {
        const newUser = new user();
        Object.assign(newUser, req.body);

        const exists = await userRepository.findByEmail(newUser.email);
        if (exists) {
            res.status(400).json({ error: 'Adresse e-mail déjà prise' });
            return;
        }

        newUser.role = 'user';
        newUser.password = await bcrypt.hash(newUser.password, 11)
        await userRepository.addUser(newUser);
        res.status(201).json(newUser);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);

    }
})

userController.post('/login', async (req, res) => {
    try {
        const user = await userRepository.findByEmail(req.body.email);
        if (user) {
            const samePassport = await bcrypt.compare(req.body.password, user.password);
            if (samePassport) {
                res.json({
                    user,
                    token: generateToken({
                        name: user.name,
                        email: user.email,
                        id: user.id,
                        role: user.role
                    })
                });
                return;
            }
        }
        res.status(401).json({ error: "E-mail et/ou mot de passe erronés" });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);

    }
});

userController.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json(req.user);
});


userController.delete('/:id', async (req,res)=>{
    try {
        await userRepository.deleteUser(req.params.id);
        res.end();
        
    } catch (error) {
        console.log(error);
        res.json(500).end();
        
    }
})