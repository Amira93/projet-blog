import { Router } from "express"
import passport from "passport";
import { article } from "../entity/article";
import { articleRepository } from "../repository/repository-article";
import { uploader } from "../uploader";


export const articleController = Router();

articleController.get('/', async (req, res) => {
    try {
        let article;
        if (req.query.search) {
            article = await articleRepository.search(req.query.search);
        } else {
            article = await articleRepository.findAllArticleUser()
        }
        res.json(article);

    } catch (error) {
        console.log(error);
        res.status(500).end();

    }
})

articleController.get('/:id', async (req, res) => {
    try {
        let data = await articleRepository.findById(req.params.id);
        res.json(data);
    } catch (error) {
        console.log(error)
        res.status(500).end();

    }

});

articleController.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let art = new article();
        Object.assign(art, req.body);
        // art.user = req.user;
        art.id_user = req.user.id;
        //  art.image= '/upload/' + req.file.filename;
        await articleRepository.addArticle(art);
        res.json(art);

    } catch (error) {
        console.log(error);
        res.status(500).end();

    }
})

articleController.put('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        await articleRepository.updateArticle(req.body);
        res.end();
    }
    catch (error) {
        console.log(error)
        res.status(500).end();
    }
});

articleController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        await articleRepository.deleteArticle(req.params.id);
        res.end();

    } catch (error) {
        console.log(error);
        res.json(500).end();

    }
})
