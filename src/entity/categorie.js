export class categorie{
    id;
    titre;
    id_titre;
    /**
     * 
     * @param {string} titre 
     * @param {number} id_titre 
     * @param {number} id 
     */

    constructor(titre, id_titre, id= null){
        this.titre = titre;
        this.id_titre = id_titre;
        this.id = id;
    }
}