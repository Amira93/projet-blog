export class article{
    id;
    titre; 
    contenu;
    id_user;
    image; 
     
 
    /**
     * 
     * @param {string} titre 
     * @param {string} contenu 
     * @param {number} id_user 
     * @param {number} id 
     */

    constructor(titre, contenu, id_user, image, id = null){
      this.titre = titre;
      this.contenu = contenu;
      this.id_user = id_user;
      this.id = id;
      this.image = image;
    }
}