
export class user{
    id;
    name;
    email;
    password;
    role;

    /**
     * 
     * @param {string} name 
     * @param {string} email 
     * @param {string} password 
     * @param {string} role 
     * @param {number} id 
     */

    constructor(name, email, password, role = "user", id = null)
    {
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.id = id;
    }
}