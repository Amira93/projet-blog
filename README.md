## Presentation de projet Blog
Projet ‘’Blog’’
Date : 17 aout 2021
Réalisateur : Amira Habi

### Description :
Un blog peut être considéré comme un “journal personnel” publié sur internet sur lequel on peut écrire d’un sujet particulier
Ainsi, publier du contenu (texte, vidéos…) sur une thématique choisie par exemple  un sujet sur une entreprise, thème à la mode ...

Crée une API d’un blog
Back-end →  Node.js / Express
Front-end →  Réact / Redux
Sécurité → Authentification avec hashing et JWT.

## Use case
![wireframe blog](image/use-case.png)
## User story
![wireframe blog](image/use-story2.png)
![wireframe blog](image/use-story1.png)
## diagramme de classe
![wireframe blog](image/diagramme-de-classe.png)
## la maquette 
![wireframe blog](image/maquette2.png)




