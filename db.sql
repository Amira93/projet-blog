DROP TABLE IF EXISTS user;
CREATE TABLE user(  
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    name VARCHAR (200) NOT NULL ,
    email VARCHAR (200) NOT NULL UNIQUE,
    password VARCHAR (100) NOT NULL ,
    role VARCHAR (200) NOT NULL  
) default charset utf8 comment '';

DROP TABLE IF EXISTS article;
CREATE TABLE `article` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `titre` varchar(200) NOT NULL,
  `contenu` varchar(300) NOT NULL,
   `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
   `id_user` int(11),
    KEY `id_user_fk` (`id_user`),
    CONSTRAINT `id_user_fk` FOREIGN KEY (`id_user`) REFERENCES user (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS categorie;
CREATE TABLE `categorie` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `titre` varchar(200) NOT NULL,
  `id_article` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_article_fk` (`id_article`),
  CONSTRAINT `id_article_fk` FOREIGN KEY (`id_article`) REFERENCES `article` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3